from django import forms

class OrderForm(forms.Form):
    name = forms.CharField(label='Name', required=True)
    tel = forms.CharField(label='Tel', required=True)