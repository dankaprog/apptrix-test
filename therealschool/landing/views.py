from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.mail import send_mail
from .forms import OrderForm

def get_form(request):
    if request.method == 'POST':
        form = OrderForm(request.POST)
        if form.is_valid():
            send_mail('New data!', str(form.cleaned_data), 'noreply@example.com',['recipient@example.com'], fail_silently=False)
            return HttpResponseRedirect('/')

    request.is_authorized = request.user.username in ["AnonymousUser",""]

    return render(request, 'index.html')

def index(request):
    return get_form(request)
